package com.e.demotest;

public class StepsModel {
    String date;
    long steps;

    public StepsModel(String date,long steps){
        this.date=date;
        this.steps=steps;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public long getSteps() {
        return steps;
    }

    public void setSteps(long steps) {
        this.steps = steps;
    }
}
