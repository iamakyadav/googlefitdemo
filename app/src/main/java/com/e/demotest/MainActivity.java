package com.e.demotest;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.Scopes;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.fitness.Fitness;
import com.google.android.gms.fitness.FitnessOptions;
import com.google.android.gms.fitness.data.Bucket;
import com.google.android.gms.fitness.data.DataPoint;
import com.google.android.gms.fitness.data.DataSet;
import com.google.android.gms.fitness.data.DataSource;
import com.google.android.gms.fitness.data.DataType;
import com.google.android.gms.fitness.data.Field;
import com.google.android.gms.fitness.request.DataReadRequest;
import com.google.android.gms.fitness.request.DataUpdateRequest;
import com.google.android.gms.fitness.result.DataReadResponse;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;


public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    public static final String TAG = "StepCounter";
    private static final int REQUEST_OAUTH_REQUEST_CODE = 12345;
    FitnessOptions fitnessOptions;

    List<StepsModel> stepsModelList;
    SimpleDateFormat sdf;

    TableLayout mTableLayout;
    ImageView imgReverse;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        stepsModelList=new ArrayList<>();
        sdf = new SimpleDateFormat("dd MMM yyyy");
        if (isInternetAvailable()) {
            initView();
        }else {
            Toast.makeText(this,"Please enable internet",Toast.LENGTH_LONG).show();
        }



    }

    private void initView() {
        imgReverse = (ImageView) findViewById(R.id.imgReverse);
        mTableLayout = (TableLayout) findViewById(R.id.table);
        mTableLayout.setStretchAllColumns(true);
        imgReverse.setOnClickListener(this);

        fitnessOptions = FitnessOptions.builder()
                .addDataType(DataType.TYPE_STEP_COUNT_DELTA, FitnessOptions.ACCESS_READ)
                .addDataType(DataType.AGGREGATE_STEP_COUNT_DELTA, FitnessOptions.ACCESS_READ)
                .build();

        GoogleSignInAccount account = GoogleSignIn.getAccountForExtension(this, fitnessOptions);

        if (!GoogleSignIn.hasPermissions(account, fitnessOptions)) {
            GoogleSignIn.requestPermissions(
                    this, // your activity
                    REQUEST_OAUTH_REQUEST_CODE, // e.g. 1
                    account,
                    fitnessOptions);
        } else {
            accessGoogleFit();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == REQUEST_OAUTH_REQUEST_CODE) {
                accessGoogleFit();
            }
        }
    }

    private void accessGoogleFit() {
        Calendar cal = Calendar.getInstance();
        cal.setTime(new Date());
        long endTime = cal.getTimeInMillis();
        cal.add(Calendar.WEEK_OF_MONTH, -2);
        long startTime = cal.getTimeInMillis();
        DataReadRequest readRequest = new DataReadRequest.Builder()
                .aggregate(DataType.TYPE_STEP_COUNT_DELTA, DataType.AGGREGATE_STEP_COUNT_DELTA)
                .setTimeRange(startTime, endTime, TimeUnit.MILLISECONDS)
                .bucketByTime(1, TimeUnit.DAYS)
                .build();

        GoogleSignInAccount account = GoogleSignIn
                .getAccountForExtension(this, fitnessOptions);

        Fitness.getHistoryClient(this, account)
                .readData(readRequest)
                .addOnSuccessListener(new OnSuccessListener<DataReadResponse>() {
                    @Override
                    public void onSuccess(DataReadResponse response) {
                        // Use response data here
                        parseResponse(response);
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.d(TAG, "OnFailure()", e);
                    }
                });
    }

    public void parseResponse(DataReadResponse dataReadResult) {
        if (dataReadResult.getBuckets().size() > 0) {
            for (Bucket bucket : dataReadResult.getBuckets()) {
                List<DataSet> dataSets = bucket.getDataSets();
                for (DataSet dataSet : dataSets) {
                    validateDataSet(dataSet);
                }
            }
        }else if (dataReadResult.getDataSets().size() > 0) {
            Log.e("History", "Number of returned DataSets: " + dataReadResult.getDataSets().size());
            for (DataSet dataSet : dataReadResult.getDataSets()) {
                validateDataSet(dataSet);
            }
        }

        Collections.reverse(stepsModelList);

        renderTableVIew();
    }

    private void validateDataSet(DataSet dataSet) {
        if (dataSet!=null) {
            long total = dataSet.isEmpty()
                    ? 0
                    : dataSet.getDataPoints().get(0).getValue(Field.FIELD_STEPS).asInt();

            for (DataPoint dp : dataSet.getDataPoints()) {
                for (Field field : dp.getDataType().getFields()) {
                    stepsModelList.add(new StepsModel(sdf.format(dataSet.getDataPoints().get(0).getTimestamp(TimeUnit.MILLISECONDS)), total));
                }

            }
        }
    }

    @Override
    public void onClick(View v) {
        if (v == imgReverse){
            if (stepsModelList!=null) {
                Collections.reverse(stepsModelList);

                renderTableVIew();
            }
        }
    }

    private void renderTableVIew() {
        if (stepsModelList != null) {
            mTableLayout.removeAllViews();
            TableRow tbrow0 = new TableRow(this);
            TextView tv0 = new TextView(this);
            tv0.setText(" Date ");
            tv0.setTextColor(Color.WHITE);
            tv0.setGravity(Gravity.CENTER);
            tv0.setBackgroundColor(Color.parseColor("#914cf3"));
            tv0.setPadding(5, 15, 0, 15);
            tbrow0.addView(tv0);
            TextView tv1 = new TextView(this);
            tv1.setText(" Total Steps ");
            tv1.setTextColor(Color.WHITE);
            tv1.setGravity(Gravity.CENTER);
            tv1.setBackgroundColor(Color.parseColor("#914cf3"));
            tv1.setPadding(5, 15, 0, 15);
            tbrow0.addView(tv1);
            mTableLayout.addView(tbrow0);
            for (int i = 0; i < stepsModelList.size(); i++) {
                TableRow tbrow = new TableRow(this);
                TextView t1v = new TextView(this);
                t1v.setText(stepsModelList.get(i).getDate());
                t1v.setTextColor(Color.BLACK);
                t1v.setGravity(Gravity.CENTER);
                t1v.setPadding(5, 15, 0, 15);
                tbrow.addView(t1v);
                TextView t2v = new TextView(this);
                t2v.setText("" + stepsModelList.get(i).getSteps());
                t2v.setTextColor(Color.BLACK);
                t2v.setGravity(Gravity.CENTER);
                t2v.setPadding(5, 15, 0, 15);
                tbrow.addView(t2v);
                mTableLayout.addView(tbrow);
            }
        }
    }

    public boolean isInternetAvailable() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        Log.d("INTERNET", "isInternetAvailable: ");
        if (netInfo != null && netInfo.isConnected()) {
            return true;
        }
        return false;
    }
}